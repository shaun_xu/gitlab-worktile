import { Application, Router } from "./deps.ts";

const router = new Router();
router.get("/ping", async ctx => {
    ctx.response.body = "PONG!";
});
router.post("/", async ctx => {
    const body = await ctx.request.body();
    console.log(JSON.stringify(body, undefined, 2));
    ctx.response.status = 202;
});

const app = new Application();

app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log(`${ctx.request.method} ${ctx.request.url} - ${ms}`);
});

app.use(router.routes());
app.use(router.allowedMethods());

console.log("http://localhost:22222/");
await app.listen({
    port: 22222
});